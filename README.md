# Lasker

A clean and modern set of chess symbols.

## Why design new symbols?

There are many freely available chess fonts (see [here](http://www.enpassant.dk/chess/fonteng.htm) and [here](http://www.chessvariants.com/d.font/)) from which one can create vector images. Merida is a particularly popular style, but like many styles, it exhibits lack of consistency across pieces and unnecessary lines that clutter the design. Previous attempts at "modern" designs, on the other hand, tend to strip away so much detail they arrive at a bland, uninspiring result, as though the pieces came from a cookie cutter.

The classic [Staunton style](https://en.wikipedia.org/wiki/Staunton_chess_set) features clean forms where each piece is clearly distinguished by shape. Lasker attempts to mimic this style, retaining the basic profile of each piece while shunning unnecessary details. Above all, Lasker aims to be easily distinguished even at very small sizes while maintaining consistency in style across all pieces.

## What's in the box?

This repository contains both SVG and PNG versions of all standard chess pieces in the `dst/` directory. (All files in `dst/` are automatically generated from a source Inkscape SVG document in `src/`.)

### White

![White Pawn](dst/png/white_pawn_64.png)
![White Knight](dst/png/white_knight_64.png)
![White Bishop](dst/png/white_bishop_64.png)
![White Rook](dst/png/white_rook_64.png)
![White Queen](dst/png/white_queen_64.png)
![White King](dst/png/white_king_64.png)

### Black

![Black Pawn](dst/png/black_pawn_64.png)
![Black Knight](dst/png/black_knight_64.png)
![Black Bishop](dst/png/black_bishop_64.png)
![Black Rook](dst/png/black_rook_64.png)
![Black Queen](dst/png/black_queen_64.png)
![Black King](dst/png/black_king_64.png)

## What can I do with Lasker?

Lasker is licensed under a [Creative Commons Attribution 4.0 International License](http://creativecommons.org/licenses/by/4.0/). This means you can use the files in this repository in any way, even for commercial products, as long as you provide attribution (e.g. by linking to this page). Here are some ideas for things you can do:

- Create a font from Lasker's SVG source.
- Use Lasker in a chess game or app.
- Expand Lasker to include fairy pieces.

The official legalese version of the license is [saved in this repository](LICENSE.md).
