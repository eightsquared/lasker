const gulp = require('gulp')
const path = require('path')

const cleandest = require('gulp-clean-dest')
const editxml = require('gulp-edit-xml')
const imagemin = require('gulp-imagemin')
const rename = require('gulp-rename')
const svg2png = require('gulp-svg2png')

const svgTasks = []
for (let color of ['white', 'black']) {
  for (let piece of ['pawn', 'knight', 'bishop', 'rook', 'queen', 'king']) {
    svgTasks.push(createSVGTask(color, piece))
  }
}
const pngTasks = []
for (let size of [32, 64, 128, 256]) {
  pngTasks.push(createPNGTask(size))
}

gulp.task('default', ['svg', 'png'])
gulp.task('svg', svgTasks)
gulp.task('png', pngTasks)

function createPNGTask(size) {
  const task = `png:${size}px`
  gulp.task(task, ['svg'], () => {
    const destPath = path.resolve(__dirname, '../dst/png')
    return gulp.src(path.resolve(__dirname, '../dst/svg/*.svg'))
      .pipe(rename((path) => {
        path.basename += `_${size}`
      }))
      .pipe(svg2png({
        height: size,
        width: size
      }))
      .pipe(imagemin([
        imagemin.optipng({
          optimizationLevel: 5
        })
      ]))
      .pipe(cleandest(destPath))
      .pipe(gulp.dest(destPath))
  })
  return task
}

function createSVGTask(color, piece) {
  const task = `svg:${color}:${piece}`
  const destPath = path.resolve(__dirname, '../dst/svg')
  const layerLabel = `${color} ${piece}`
  gulp.task(task, () => {
    return gulp.src(path.resolve(__dirname, '../src/lasker.svg'))
      .pipe(rename(`${color}_${piece}.svg`))
      .pipe(editxml((xml) => {
        for (let i = xml.svg.g.length - 1; i >= 0; i--) {
          const layer = xml.svg.g[i]
          if (layer.$['inkscape:label'] !== layerLabel) {
            xml.svg.g.splice(i, 1)
          }
        }
        return xml
      }))
      .pipe(imagemin([
        imagemin.svgo({
          plugins: [
            {
              removeAttrs: {
                attrs: [
                  'style',
                  'font-weight',
                  'font-family',
                  'overflow',
                  'white-space'
                ]
              }
            }
          ]
        })
      ]))
      .pipe(cleandest(destPath))
      .pipe(gulp.dest(destPath))
  })
  return task
}
